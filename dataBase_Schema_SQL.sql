CREATE DATABASE IF NOT EXISTS fs1030_individual;
USE fs1030_individual;
SET
  SQL_SAFE_UPDATES = 0;
CREATE TABLE `portfolio` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    notes varchar(255) NOT NULL,
    image varchar(255),
    PRIMARY KEY (id)
  ) ENGINE = InnoDB DEFAULT CHARSET = latin1;
CREATE TABLE `contact` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    email varchar(255) NOT NULL,
    phone varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    message varchar(255) NOT NULL,
    PRIMARY KEY (id)
  ) ENGINE = InnoDB DEFAULT CHARSET = latin1;
CREATE TABLE `users` (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(255) NOT NULL,
    PRIMARY KEY (id)
  ) ENGINE = InnoDB DEFAULT CHARSET = latin1;
INSERT INTO
  portfolio (title, notes, image)
VALUES
  (
    'First Work ',
    'First ever website',
    'N/A'
  ),
  (
    'Second Work ',
    'Second ever work',
    'N/A'
  ),
  (
    'Third Work',
    'Third ever work',
    'N/A'
  );
INSERT INTO
  users (
    name,
    email,
    username,
    password,
    role
  )
VALUES
  (
    'Admin User ',
    'admin@gmail.com',
    'admin',
    '$2a$08$ReAz9h4djdosNK91FKH0HeIIApH0u17vAJQ2JgIdEEAfua2PZL.D.',
    'admin'
  )